// this script checks the equality of the 'sources' of the wcp configuration between (old) bicatcgi.conf and (new) config in wisecatplusconfig table

// run this script on machine where wise-bms-base is installed

const fs = require('fs')
const _ = require('underscore')
const request = require('request')
const mysql = require('mysql')


// Usage: node index.js [webserver url]
// E.g. (test machine) node index.js http://localhost
// E.g. (https in prod machine) node index.js https://webcat.fbn-net.nl

doProcess({
  webserver : process.argv[2] || 'http://webserver'
})


async function doProcess(context) {
  try {
    const dbconfig = await getMbConfiguration()
    const branches = await fetchBranches(dbconfig)
    let timer = setInterval(() => {
      let branch = branches.pop()
      if (!branch) {
        return clearInterval(timer)
      }
      checkBranch(context, branch.vestiging)
    }, 500)
  }
  catch(exception) {
    console.error(`Error during fetch mysql configuration`, exception)
  }
}


function fetchBranches(config) {
  return new Promise((resolve, reject) => {
    let connection = mysql.createConnection({
      host     : config.DBHOST
     ,user     : config.DBUSER
     ,password : config.DBPASS
     ,database : config.DBDATA
    })
    connection.connect()
    connection.query('SELECT vestiging FROM vestiging', (error, branches) => {
      if (error) return reject(error)
      resolve(branches)
    })
    connection.end()
  })
}


// returns { DBHOST:"", DBDATA:"", ... }
async function getMbConfiguration() {
  return new Promise((resolve, reject) => {
    fs.readFile('/home/hka/bxmdb/SQL/schema_bicat.cfg', 'utf8', (error, data) => {
      if (error) return reject(error)
      let config = {}
      data.split('\n').forEach((row) => {
        if (!row) return
        let items = row.split('=')
        config[items[0]] = items[1].replace(/"/g, '')
      })
      resolve(config)
    })
  })
}


async function checkBranch(context, branchid) {
  try {
    const uris = createComparableWcpConfigUris(context, branchid)
    const config1 = await getWcpConfigObject(uris[0])
    const config2 = await getWcpConfigObject(uris[1])
    console.info(`Is 'sources' config equal for ${branchid}?`, _.isEqual(config1.sources, config2.sources))
  }
  catch(exception) {
    console.error(`Could not check wcp config for branch ${branchid}`, exception)
  }
}


// returns [ uri1, uri2 ]
function createComparableWcpConfigUris(context, branchid) {
  const uri = `${context.webserver}/cgi-bin/bx.pl?fmt=json&event=svc-cfg&services=search,availability&var=portal&vestnr=${branchid}&prt=INTERNET`
  return [ uri, uri+'&catalog=default' ]
}


// returns {}
function getWcpConfigObject(uri) {
  console.info(`Fetch wcp config from ${uri}`)
  return new Promise((resolve, reject) => {
    request(uri, (error, response, body) => {
      if (error) return reject(error)
      try {
        resolve(JSON.parse(body))
      }
      catch(exception) {
        console.error(`Error parsing response of ${uri}`, exception)
        reject()
      }
    })
  })
}


